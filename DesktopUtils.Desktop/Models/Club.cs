﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopUtils.Desktop.Models
{
    public class Club
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public override string ToString()
        {
            return string.Format("Club Id: {0}, Name: {1} {2}", this.Id, this.Name, this.Description);
        }
    }
}

/*
 * 
 *  public string XXXEntityPluralXXX { get => GetItemToReplace("XXXEntityPluralXXX"); } // txtCapitalPlural.Text
    public string XXXEntityLowerPluralXXX { get => GetItemToReplace("XXXEntityLowerPluralXXX"); } // txtCamelPlural.Text
    public string XXXEntitySingularXXX { get => GetItemToReplace("XXXEntitySingularXXX"); } //  txtCapitalSingular.Text);
    public string XXXEntityLowerSingularXXX { get => GetItemToReplace("XXXEntityLowerSingularXXX"); } // txtCamelSingular.Text


    XXXFieldsDetailsTplXXX = //XXXFieldsDetailsTplXXX

    XXXFieldsDetailsDesigner1TplXXX = //XXXFieldsDetailsDesigner1TplXXX

    XXXFieldsDetailsDesigner2TplXXX = //XXXFieldsDetailsDesigner2TplXXX

    XXXFieldsDetailsDesigner2TplXXX = //XXXFieldsDetailsDesigner2TplXXX

    XXXFieldsDetailsDesigner2TplXXX = //XXXFieldsDetailsDesigner2TplXXX

    XXXFieldsEdit1TplXXX = //XXXFieldsEdit1TplXXX

    XXXFieldsEdit2TplXXX = //XXXFieldsEdit2TplXXX

    XXXFieldsIndexDesigner1TplTXXX  = //XXXFieldsIndexDesigner1TplTXXX 

    XXXFieldNameXXX


 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
