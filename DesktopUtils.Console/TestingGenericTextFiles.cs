﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesktopUtils.Core;

namespace DesktopUtils.Console
{
    public class TestingGenericTextFiles
    {
        public static void Run()
        {
            var club1 = new Club
            {
                Address = "Centennial College",
                Id = 1,
                Description = "Description yes 1",
                ModifiedDate = DateTime.Now,
                Name = "Club Name"
            };
            var club2 = new Club
            {
                Address = "Centennial College2 2",
                Id = 2,
                Description = "Description yes 2",
                ModifiedDate = DateTime.Now,
                Name = "Club Name"
            };
            var obj = new GenericRepositoryFiles<Club>(true);
            ReadKey();
            obj.Add(club1);
            obj.Add(club2);

            WriteLine("From Bucle");
            foreach (var item in obj.List)
            {
                WriteLine(item.ToString());
            }

            ReadKey();

            WriteLine("From Get Club");
            WriteLine(obj.Get(1).ToString());

            obj.Update(new Club
            {
                Id = 2,
                Address = "Other Address",
                Description = "Other Description",
                ModifiedDate = DateTime.Now,
                Name = "Other Name1"
            });

            WriteLine("From Bucle again");
            foreach (var item in obj.List)
            {
                WriteLine(item.ToString());
            }

            ReadKey();

            obj.Delete(1);

            WriteLine("From Bucle again");
            foreach (var item in obj.List)
            {
                WriteLine(item.ToString());
            }

            ReadKey();

        }

        public class Club
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Address { get; set; }
            public DateTime? ModifiedDate { get; set; }
            public override string ToString()
            {
                return string.Format("Club Id: {0}, Name: {1} {2}", this.Id, this.Name, this.Description);
            }
        }
    }
}
