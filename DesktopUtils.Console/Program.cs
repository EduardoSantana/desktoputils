﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DesktopUtils.Console
{


    public enum EMonth
    {
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER
    }


    class Program
    {
        static void Main(string[] args)
        {

            WriteLine(EMonth.JANUARY.ToString());
            ReadKey();
            //TestingGenericTextFiles.Run();
            
        }
    }
}
