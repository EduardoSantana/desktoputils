﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopUtils.Core
{
    public class FormHelpers
    {
        
        public static void LoadComboBox<T>(GenericRepositoryFiles<T> repo, string displayMember, ComboBox combo, bool orderByDisplayMember = true)
        {
            var dt = new List<dynamic>(); 

            foreach (var item in repo.List)
            {
                dt.Add(new { Id = repo.GetValue(item), Name = repo.GetValue(item, displayMember) });
            }

            if (orderByDisplayMember)
            {
                dt.OrderBy(p=>p.Name);
            }

            combo.DataSource = dt.ToArray();
            combo.DisplayMember = "Name";
            combo.ValueMember = "Id";
        }

        public static string ObtenerNombreMaquina()
        {
            string searchCharacter = ".";
            string name = System.Net.Dns.GetHostName();
            if (name.Contains(searchCharacter))
            {
                int UltimoIndice = name.IndexOf(searchCharacter);
                name = name.Substring(0, UltimoIndice);
            }
            return name;
        }

        public static string AddNameAndValue(string name, string value)
        {
            return string.Format("{0}={1},", name, value);
        }

    }
}
