﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DesktopUtils.Core
{
    public class GenericRepositoryFilesXML<T>
    {
        private string FILENAME { get; set; }

        private List<T> Ts { get; set; }

        public GenericRepositoryFilesXML(bool dropfile = false)
        {
            FILENAME = typeof(T).Name + "_xml.txt";
            if (dropfile && File.Exists(FILENAME))
            {
                File.Delete(FILENAME);
                Thread.Sleep(900);
            }
            Ts = GetJsonList();
        }

        public IQueryable<T> List => Ts.AsQueryable<T>();
               
        public void Add(T obj)
        {
            var id = 1;
            if (Ts.Any())
            {
                id = Ts.Max(p => GetValue(p)) + 1;
            }
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.Name == "Id")
                {
                    prop.SetValue(obj, id);
                }
                if (prop.Name == "ModifiedDate")
                {
                    prop.SetValue(obj, DateTime.Now);
                }
            }

            Ts.Add(obj);
            SaveList();
        }

        public void Update(T obj)
        {
            var saved = Get(GetValue(obj));
            foreach (var prop in saved.GetType().GetProperties())
            {
                foreach (var prop_to_save in obj.GetType().GetProperties())
                {
                    if (prop.Name == prop_to_save.Name)
                    {
                        prop.SetValue(saved, prop_to_save.GetValue(obj));
                    }
                }

                if (prop.Name == "ModifiedDate")
                {
                    prop.SetValue(obj, DateTime.Now);
                }

            }

            SaveList();
        }

        public void Delete(int id)
        {
            var obj = Get(id);
            Ts.Remove(obj);
            SaveList();
        }

        public T Get(int id)
        {
            return Ts.FirstOrDefault(p=>GetValue(p) == id);
        }

        public int GetValue(T obj, string propertyName = "Id")
        {
            var property = obj.GetType().GetProperties().FirstOrDefault(p=>p.Name == propertyName);
            var retVal = property.GetValue(obj).ToString();
            if (string.IsNullOrEmpty(retVal))
            {
                retVal = "0";
            }
            return int.Parse(retVal);
        }

        public Result GetValue<Result>(T obj, string propertyName = "Id")
        {
            var property = obj.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName);
            Result retVal = (Result)property.GetValue(obj);
            return retVal;
        }

        private void SaveList()
        {
            var send = new DataToSave() { List = Ts };
            SetStringToFile(GetXMLFromObject(send));
        }

        private List<T> GetJsonList()
        {
            var retVal = new DataToSave() { List = new List<T>() };

            if (!string.IsNullOrEmpty(GetStringFromFile()))
            {
                retVal = ObjectToXML(GetStringFromFile());
            }

            return retVal.List;
        }

        private void SetStringToFile(string message, bool append = false)
        {
            var streamWriter = new StreamWriter(FILENAME, append);
            streamWriter.WriteLine(message);
            streamWriter.Close();
        }

        private string GetStringFromFile()
        {
            if (File.Exists(FILENAME))
            {
                var fileStream = new FileStream(FILENAME, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
            else
            {
                return "";
            }
            
        }
        public static DataToSave ObjectToXML(string xml)
        {
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            DataToSave obj = new DataToSave();
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(typeof(DataToSave));
                xmlReader = new XmlTextReader(strReader);
                obj = (DataToSave)serializer.Deserialize(xmlReader);
            }
            catch (Exception exp)
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            return obj;
        }

        public static string GetXMLFromObject(DataToSave o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        public class DataToSave
        {
            public List<T> List { get; set; }
        }

    }
}
