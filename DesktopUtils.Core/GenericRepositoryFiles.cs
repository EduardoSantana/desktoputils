﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DesktopUtils.Core
{
    public class GenericRepositoryFiles<T>
    {
        private string FILENAME { get; set; }

        private List<T> Ts { get; set; }

        public GenericRepositoryFiles(bool dropfile = false)
        {
            FILENAME = typeof(T).Name + ".txt";
            if (dropfile && File.Exists(FILENAME))
            {
                File.Delete(FILENAME);
                Thread.Sleep(900);
            }
            Ts = GetJsonList();
        }

        public IQueryable<T> List => Ts.AsQueryable<T>();
               
        public void Add(T obj)
        {
            var id = 1;
            if (Ts.Any())
            {
                id = Ts.Max(p => GetValue(p)) + 1;
            }
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.Name == "Id")
                {
                    prop.SetValue(obj, id);
                }
                if (prop.Name == "ModifiedDate")
                {
                    prop.SetValue(obj, DateTime.Now);
                }
            }

            Ts.Add(obj);
            SaveList();
        }

        public void Update(T obj)
        {
            var saved = Get(GetValue(obj));
            foreach (var prop in saved.GetType().GetProperties())
            {
                foreach (var prop_to_save in obj.GetType().GetProperties())
                {
                    if (prop.Name == prop_to_save.Name)
                    {
                        prop.SetValue(saved, prop_to_save.GetValue(obj));
                    }
                }

                if (prop.Name == "ModifiedDate")
                {
                    prop.SetValue(obj, DateTime.Now);
                }

            }

            SaveList();
        }

        public void Delete(int id)
        {
            var obj = Get(id);
            Ts.Remove(obj);
            SaveList();
        }

        public T Get(int id)
        {
            return Ts.FirstOrDefault(p=>GetValue(p) == id);
        }

        public int GetValue(T obj, string propertyName = "Id")
        {
            var property = obj.GetType().GetProperties().FirstOrDefault(p=>p.Name == propertyName);
            var retVal = property.GetValue(obj).ToString();
            if (string.IsNullOrEmpty(retVal))
            {
                retVal = "0";
            }
            return int.Parse(retVal);
        }

        public Result GetValue<Result>(T obj, string propertyName = "Id")
        {
            var property = obj.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName);
            Result retVal = (Result)property.GetValue(obj);
            return retVal;
        }

        private void SaveList()
        {
            SetStringToFile(JsonConvert.SerializeObject(Ts));
        }

        private List<T> GetJsonList()
        {
            return string.IsNullOrEmpty(GetStringFromFile()) ?
                new List<T>() :
                JsonConvert.DeserializeObject<List<T>>(GetStringFromFile());

        }

        private void SetStringToFile(string message, bool append = false)
        {
            var streamWriter = new StreamWriter(FILENAME, append);
            streamWriter.WriteLine(message);
            streamWriter.Close();
        }

        private string GetStringFromFile()
        {
            if (File.Exists(FILENAME))
            {
                var fileStream = new FileStream(FILENAME, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    return streamReader.ReadToEnd();
                }
            }
            else
            {
                return "";
            }
            
        }

    }
}
