﻿using System.Data;
using System.Windows.Forms;

namespace DesktopUtils.Core
{
    public static class FormExtentions
    {
        public static string FORMATWITHDECIMALS = "#,##0.00";
        public static string FORMATWITHNODECIMALS = "#,##0";
        public static string FORMATDATE = "MM/dd/yyyy";

        public static void AssignData(this DataGridView o, object[] tb)
        {
            o.AutoGenerateColumns = false;
            o.DataSource = null;
            o.DataSource = tb;
        }

        public static string GetColumnName(this DataGridView o, int id)
        {
            return o.Columns[id].Name;
        }

        public static string GetRowValue(this DataGridView o, int rowId, string columnId)
        {
            return o.Rows[rowId].Cells[columnId].Value.ToString();
        }

        public static bool HasRecords(this DataTable o)
        {
            return (o.Rows.Count > 0);
        }

        public static void FormatearNumeroCon2DecimalesEnColumna(this DataGridView DataGridView, DataGridViewTextBoxColumn columna)
        {
            DataGridView.Columns[columna.Name].DefaultCellStyle.Format = FORMATWITHDECIMALS;
            DataGridView.Columns[columna.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        public static void FormatearNumeroEntero(this DataGridView DataGridView, DataGridViewTextBoxColumn columna)
        {
            DataGridView.Columns[columna.Name].DefaultCellStyle.Format = FORMATDATE;
            DataGridView.Columns[columna.Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

    }
}
