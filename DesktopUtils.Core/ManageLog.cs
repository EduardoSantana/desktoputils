﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopUtils.Core
{
    public class ManageLog
    {
        public const string FILENAME = "Log.txt";
        public const string DELIM = " | ";
        
        public static void Add(params string[] p)
        {
            string message = "";
            foreach (var item in p)
            {
                message += item + DELIM;
            }
            message = message.Substring(0, message.Length - DELIM.Length);

            var streamWriter = new StreamWriter(FILENAME, true);
            streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + DELIM + message + DELIM + "{ENDLINE}");
            streamWriter.Close();
        }

        public static void Add(Exception ex, params string[] p)
        {
            string message = "";
            message += ex.Message + DELIM;
            foreach (var item in p)
            {
                message += item + DELIM;
            }
            message = message.Substring(0, message.Length - DELIM.Length);

            var streamWriter = new StreamWriter(FILENAME, true);
            streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + DELIM + message + DELIM + "{ENDLINE}");
            streamWriter.Close();
        }

    }

   
}
