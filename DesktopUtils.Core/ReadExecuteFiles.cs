﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopUtils.Core
{
    public class ReadExecuteFiles
    {
        public const string FILENAME = "Log.txt";

        public const string DELIM = " | ";

        public static List<string> ReadFiles()
        {
            var retVal = new List<string>();
            foreach (var item in Directory.GetFiles(Directory.GetCurrentDirectory()))
            {
                if (item.Contains(".bat"))
                {
                    retVal.Add(item);
                }
            }
            return retVal;
        }

        public static void ExecuteCommand(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + "\"" + command + "\"");
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            SaveLog(CreateMessageToLog("Execute", exitCode.ToString(), output, error, command));

            process.Close();
        }

        public static void ExecuteFiles(List<string> l)
        {
            foreach (var item in l)
            {
                try
                {
                    ExecuteCommand(item);
                }
                catch (Exception ex)
                {
                    SaveLog("Message" + DELIM + ex.Message + DELIM + "Error File" + DELIM + item);
                }
            }
        }

        //public static string ReadLog()
        //{
        //    var fileStream = new FileStream(@"c:\file.txt", FileMode.Open, FileAccess.Read);
        //    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
        //    {
        //        text = streamReader.ReadToEnd();
        //    }
        //}

        public static void SaveLog(string message)
        {
            var streamWriter = new StreamWriter(FILENAME, true);
            streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + DELIM + message + DELIM + "{ENDLINE}");
            streamWriter.Close();
        }

        public static string CreateMessageToLog(string message, string code, string output, string error, string file)
        {
            return message + DELIM + code + (string.IsNullOrEmpty(output) ? "" : DELIM + output) + (string.IsNullOrEmpty(error) ? "" : DELIM + "Error:" + error) + (string.IsNullOrEmpty(file) ? "" : DELIM + file);
        }

    }

}
