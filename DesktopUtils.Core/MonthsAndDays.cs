﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopUtils.Core
{
    public class MonthsAndDays
    {
        public enum EDay
        {
            SUNDAY,
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY
        }

        public enum EMonth
        {
            JANUARY,
            FEBRUARY,
            MARCH,
            APRIL,
            MAY,
            JUNE,
            JULY,
            AUGUST,
            SEPTEMBER,
            OCTOBER,
            NOVEMBER,
            DECEMBER
        }

        public static List<string> GetMonths()
        {
            var retVal = new List<string>();
            foreach (string day in Enum.GetNames(typeof(EDay)))
                retVal.Add(day);
            return retVal;
        }

        public static List<string> GetDays()
        {
            var retVal = new List<string>();
            foreach (string day in Enum.GetNames(typeof(EDay)))
                retVal.Add(day);
            return retVal;
        }

        public static string GetDay(int id)
        {
            if (id > 7 || id < 1)
                return "";
            return Enum.GetNames(typeof(EDay))[id];
        }

        public static string GetMonth(int id)
        {
            if (id > 12 || id < 1)
                return "";
            return Enum.GetNames(typeof(EMonth))[id - 1];
        }

        public static DateTime DateBegin(DateTime d)
        {
            return new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
        }

        public static DateTime DateEnd(DateTime d)
        {
            return new DateTime(d.Year, d.Month, d.Day, 23, 59, 59);
        }
    }
}
