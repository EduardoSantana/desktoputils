﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DesktopUtils.Core
{
    public class SMTPMailSending
    {
        public static bool SendMail(string From, 
                                string Password, 
                                string to, 
                                string Subject,
                                string Body, 
                                bool DebeIrEnHTML, 
                                string txtSmtpClient, 
                                string txtPort)
        {
            bool Enviado = false;
            try
            {
                //EtiquetaMensajeError.Text = "";
                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = DebeIrEnHTML;

                //Se ponen las direcciones
                mail.From = new MailAddress(From); //IMPORTANT: This must be same as your smtp authentication address.
                mail.To.Add(to);

                //Se pone el contenido
                mail.Subject = Subject;
                mail.Body = Body;
                //
                SmtpClient smtp = new SmtpClient(txtSmtpClient);
                smtp.Port = int.Parse(txtPort);
                smtp.UseDefaultCredentials = false;

                //IMPORANT:  Your smtp login email MUST be same as your FROM address. 
                NetworkCredential Credentials = new NetworkCredential(From, Password);
                smtp.Credentials = Credentials;
                smtp.EnableSsl = true;
                smtp.Send(mail);
                Enviado = true;
                // EtiquetaMensajeError.Text = "Mensaje Enviado Exitosamente";
            }
            catch (Exception ex)
            {
                ManageLog.Add(ex, "Sending Mail - SM001");
            }
            return Enviado;
        }
    }
}
